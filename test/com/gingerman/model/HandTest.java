package com.gingerman.model;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.*;

/**
 * Created by dale on 1/20/2014.
 */
public class HandTest {

    @Test
    public void testExpectedConstructors() {
        // Basic creating of cards using a Set
        List<Card> cards = new ArrayList<Card>(5);
        cards.add(new Card("ks"));
        cards.add(new Card("qs"));
        cards.add(new Card("js"));
        cards.add(new Card("10s"));
        cards.add(new Card("as"));
        Hand expectedSetHand = new Hand(cards);
        assertNotNull("Expected Set Hand is null", expectedSetHand);

        // Basic creating of cards using valid shorthand
        Hand expectedShorthandHand = new Hand("as,ks,qs,js,10s");
        assertNotNull("Expected shorthand Hand is null", expectedShorthandHand);
    }

    @Test
    public void testUnexpectedConstructors() {
        // incorrect Set based
        List<Card> cards = new ArrayList<Card>(4);
        cards.add(new Card("as"));
        cards.add(new Card("ks"));
        cards.add(new Card("qs"));
        cards.add(new Card("js"));
        try {
            Hand expectedSetHand = new Hand(cards);
            fail("Hand constructor with four card set accepted");
        } catch (IllegalArgumentException e) {
            // expected to be thrown!
        }


        // incorrect shorthands
        try {
            Hand hand = new Hand("");
            fail("empty shorthand accepted in Hand constructor");
        } catch(IllegalArgumentException e) {
            // expected to be thrown!
        }
        try {
            Hand hand = new Hand("aS");
            fail("too few Cards accepted in Hand constructor");
        } catch(IllegalArgumentException e) {
            // expected to be thrown!
        }
        try {
            Hand hand = new Hand("as,ks,qs,js,11s"); // "js" and "11s" evaluate to the same
            fail("duplicate Card accepted in Hand constructor");
        } catch(IllegalArgumentException e) {
            // expected to be thrown!
        }
    }

    @Test
    public void testHandRanking() {
        /*
          Test all rank types are ranked successfully
         */
        Hand royalFlush = new Hand("10s,11s,12s,13s,14s");
        assertTrue("Royal Flush not ranked properly", royalFlush.rank == Hand.RANK.ROYAL_FLUSH);

        Hand straightFlush = new Hand("10c,9c,8c,7c,6c");
        assertTrue("Straight Flush not ranked properly", straightFlush.rank == Hand.RANK.STRAIGHT_FLUSH);

        Hand four = new Hand("2s,2d,2c,2h,as");
        assertTrue("Four of a Kind not ranked properly", four.rank == Hand.RANK.FOUR_OF_A_KIND);

        Hand fullHouse1 = new Hand("3s,3d,3c,4s,4d");
        assertTrue("Full House (higher pair) not ranked properly", fullHouse1.rank == Hand.RANK.FULL_HOUSE);
        Hand fullHouse2 = new Hand("3s,3d,4c,4s,4d");
        assertTrue("Full House (lower pair) not ranked properly", fullHouse2.rank == Hand.RANK.FULL_HOUSE);

        Hand flush = new Hand("14s,12s,10s,8s,6s");
        assertTrue("Flush not ranked properly", flush.rank == Hand.RANK.FLUSH);

        Hand straight = new Hand("2c,3d,4c,5c,6c");
        assertTrue("Straight not ranked properly", straight.rank == Hand.RANK.STRAIGHT);

        Hand three = new Hand("8h,8s,8c,6c,3c");
        assertTrue("Three of a Kind not ranked properly", three.rank == Hand.RANK.THREE_OF_A_KIND);

        Hand twoPair = new Hand("11h,11c,2h,2s,4c");
        assertTrue("Two Pair not ranked properly", twoPair.rank == Hand.RANK.TWO_PAIR);

        Hand pair = new Hand("12h,12c,8c,7c,6c");
        assertTrue("One Pair not ranked properly", pair.rank == Hand.RANK.ONE_PAIR);

        Hand highCard = new Hand("14c,2s,3c,4s,5c");
        assertTrue("High Card not ranked properly", highCard.rank == Hand.RANK.HIGH_CARD);

        /*
          Verify that the different ranks can be compared as expected
         */
        assertTrue("Royal Flush not ranked higher than Straight Flush", royalFlush.compareTo(straightFlush) >= 0);
        assertTrue("High Card not ranked lower than Full House", highCard.compareTo(fullHouse1) <= 0);
    }

    @Test
    public void testCompareLikeRank() {
        // Royal Flushes
        assertTrue("Two Royal Flushes should compare with equal priority", new Hand("10s,11s,12s,13s,14s").compareTo(new Hand("10c,11c,12c,13c,14c")) == 0);

        // Straight Flushes
        Hand sf1 = new Hand("12s,11s,10s,9s,8s");
        Hand sf2 = new Hand("11c,10c,9c,8c,7c");
        assertHigherLower(sf1, sf2);

        // Four of a Kind
        Hand fourLowFifth = new Hand("8s,8c,8d,8h,6s");
        Hand fourHighFifth = new Hand("8s,8c,8d,8h,as");
        Hand fourLow = new Hand("4s,4c,4d,4h,as");
        assertHigherLower(fourHighFifth, fourLowFifth);
        assertHigherLower(fourLowFifth, fourLow);

        // Full House
        Hand fullLow = new Hand("4s,4h,4c,as,ah");
        Hand fullHigh = new Hand("as,ah,ac,4s,4h");
        assertHigherLower(fullHigh, fullLow);

        // Flush
        Hand flushLow = new Hand("2s,3s,5s,6s,7s");
        Hand flushHigh = new Hand("2h,3h,5h,6h,ah");
        assertHigherLower(flushHigh, flushLow);

        // Straight
        Hand straightLow = new Hand("2s,3h,4h,5h,6h");
        Hand straightHigh = new Hand("5s,6h,7h,8h,9h");
        assertHigherLower(straightHigh, straightLow);

        // Three of a Kind
        Hand threeLow = new Hand("2c,2s,2h,ah,as");
        Hand threeHigh = new Hand("ac,as,ah,2h,2s");
        assertHigherLower(threeHigh, threeLow);

        // Two Pair
        Hand twoPairLow = new Hand("2c,2h,3c,3h,4s");
        Hand twoPairMid = new Hand("2c,2h,5c,5h,4s");
        Hand twoPairHigh = new Hand("5c,5h,ac,ah,4s");
        assertHigherLower(twoPairMid, twoPairLow);
        assertHigherLower(twoPairHigh, twoPairMid);

        // One Pair
        Hand pairLow = new Hand("2c,2h,8h,9h,10h");
        Hand pairMid = new Hand("2c,2h,11h,12h,13h");
        Hand pairHigh = new Hand("ac,ad,2h,3h,4h");
        assertHigherLower(pairMid, pairLow);
        assertHigherLower(pairHigh, pairMid);

        // High Card
        Hand highCardLow = new Hand("2s,4s,6s,8h,10h");
        Hand highCardHigh = new Hand("2h,4h,6h,10s,as");
        assertHigherLower(highCardHigh, highCardLow);
        Hand highCardSameValue = new Hand("2c,4c,6c,8d,10d");
        assertTrue("High Cards with the same cards should compare to zero", highCardLow.compareTo(highCardSameValue) == 0);

        // Test equality
        assertEquals("Hands with identical cards should be equal", new Hand("as,4s,6s,8h,10h"), new Hand("10h,14s,6s,4s,8h"));
    }

    /** Helper to simplify some common tests */
    private void assertHigherLower(Hand higher, Hand lower) {
        assertTrue(String.format("%s should be higher than %s", higher.toString(), lower.toString()), higher.compareTo(lower) > 0);
        assertTrue(String.format("%s should be lower than %s", lower.toString(), higher.toString()), lower.compareTo(higher) < 0);
    }
}
