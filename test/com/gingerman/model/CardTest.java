package com.gingerman.model;

import com.gingerman.model.Card;
import org.junit.Test;

import java.util.Arrays;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.fail;
import static org.junit.Assert.assertTrue;

/**
 * Unit test for Card class
 */
public class CardTest {

    @Test
    public void testConstructors() {
        // basic creation of card using enums
        Card cardEnums = new Card(Card.VALUE.FOUR, Card.SUIT.HEART);
        assertNotNull("Card created with enums is null", cardEnums);

        // basic creation of card using valid shorthand string
        Card cardValidShorthand = new Card("9h");
        assertNotNull("Card created with valid shorthand is null", cardValidShorthand);
        assertEquals("Card create with valid shorthand: value interpretation", Card.VALUE.NINE, cardValidShorthand.value);
        assertEquals("Card create with valid shorthand: suit (lowercase) interpretation", Card.SUIT.HEART, cardValidShorthand.suit);

        // basic creation of card using valid shorthand string, testing face card values
        Card cardValidFacecardNumeric = new Card("12S");
        assertNotNull("Card created with valid facecard numeric is null", cardValidFacecardNumeric);
        assertEquals("Card created with valid facecard numeric: value interpretation", Card.VALUE.QUEEN, cardValidFacecardNumeric.value);
        assertEquals("Card created with valid facecard numeric: suit (uppercase) interpretation", Card.SUIT.SPADE, cardValidFacecardNumeric.suit);
        Card cardValidFacecardAlpha = new Card("qs");
        assertNotNull("Card created with valid facecard alpha is null", cardValidFacecardAlpha);
        assertEquals("Card created with valid facecard alpha: value interpretation", Card.VALUE.QUEEN, cardValidFacecardAlpha.value);
        assertEquals("Card created with valid facecard alpha: suit (lowercase) interpretation", Card.SUIT.SPADE, cardValidFacecardAlpha.suit);

        // invalid shorthands
        try {
            Card card = new Card("1h");
            fail("Card created with invalid value (1) did not throw");
        } catch(IllegalArgumentException e) {
            // expected to be thrown!
        }
        try {
            Card card = new Card("15h");
            fail("Card created with invalid value (15) did not throw");
        } catch(IllegalArgumentException e) {
            // expected to be thrown!
        }
        try {
            Card card = new Card("h");
            fail("Card created with missing value did not throw");
        } catch(IllegalArgumentException e) {
            // expected to be thrown!
        }
        try {
            Card card = new Card("2z");
            fail("Card created with invalid suit (z) did not throw");
        } catch(IllegalArgumentException e) {
            // expected to be thrown!
        }
        try {
            Card card = new Card("2");
            fail("Card created with missing suit did not throw");
        } catch(IllegalArgumentException e) {
            // expected to be thrown!
        }
        try {
            Card card = new Card("2zz");
            fail("Card created with invalid shorthand (2zz) did not throw");
        } catch(IllegalArgumentException e) {
            // expected to be thrown!
        }
        try {
            Card card = new Card("");
            fail("Card created with invalid shorthand ('') did not throw");
        } catch(IllegalArgumentException e) {
            // expected to be thrown!
        }
        try {
            Card card = new Card(null);
            fail("Card created with null shorthand did not throw");
        } catch(IllegalArgumentException e) {
            // expected to be thrown!
        }
    }

    @Test
    public void testCompareTo() {
        Card lowCard = new Card("2h");
        Card midCard = new Card("7c");
        Card highCard = new Card("As");
        Card[] unsorted = new Card[]{midCard, lowCard, highCard};

        // verify sorting works as expected
        Arrays.sort(unsorted);
        assertTrue("Low card not sorted to first position", unsorted[0].equals(lowCard));
        assertTrue("High card not sorted to last position", unsorted[2].equals(highCard));

        // verify equality test
        Card highCardDuplicate = new Card("14s");
        assertEquals("Equal cards are not evaluated as equal", highCard, highCardDuplicate);
    }
}
