package com.gingerman.model;

/**
 * Represents a playing card and allows for comparing with other Cards.
 */
public class Card implements Comparable<Card> {

    public VALUE value;
    public SUIT suit;

    // Possible card (numeric) values
    public enum VALUE {
        TWO(new String[]{"2"}),
        THREE(new String[]{"3"}),
        FOUR(new String[]{"4"}),
        FIVE(new String[]{"5"}),
        SIX(new String[]{"6"}),
        SEVEN(new String[]{"7"}),
        EIGHT(new String[]{"8"}),
        NINE(new String[]{"9"}),
        TEN(new String[]{"10"}),
        JACK(new String[]{"j","11"}),
        QUEEN(new String[]{"q","12"}),
        KING(new String[]{"k","13"}),
        ACE(new String[]{"a","14"}),;

        private String[] mKeys;
        private VALUE(String[] keys) {
            mKeys = new String[keys.length];
            for (int i = 0; i < keys.length; i++) {
                mKeys[i] = keys[i];
            }
        }

        /**
         * @param key key of the value being sought after
         * @return the VALUE represented by this key
         * @throws IllegalArgumentException if the key specified does not map to a VALUE
         */
        public static VALUE getByKey(String key) throws IllegalArgumentException {
            for (VALUE currValue : VALUE.values()) {
                for (String definedKey : currValue.mKeys) {
                    if (definedKey.equals(key.toLowerCase())) return currValue;
                }
            }
            throw new IllegalArgumentException("Card value could not be found for key: "+key);
        }
    }
    // Possible card suits
    public enum SUIT {
        HEART("h"),
        SPADE("s"),
        DIAMOND("d"),
        CLUB("c");

        private String mKey;
        private SUIT(String key) {
            mKey = key;
        }

        /**
         * @param key key of the suit being sought after
         * @return the SUIT represented by the key
         * @throws IllegalArgumentException if the key specified does not map to a SUIT
         */
        public static SUIT getByKey(String key) throws IllegalArgumentException {
            for (SUIT suit : SUIT.values()) {
                if (suit.mKey.equals(key.toLowerCase())) return suit;
            }
            throw new IllegalArgumentException("Card suit could not be found for key: "+key);
        }
    }

    /**
     * Accepts a shorthand String representation representing a cards value and suit, parsed as a numeric or letter
     * representation of the card value (ex. "9","J","11") prefixing a single letter representing the suit ("H","S","D","C").
     * Examples:  "9H" represents nine of hearts. "11S" and "JS" both represent the Jack of Spades. All letters are case insensitive.
     * @param shorthand shorthand definition of the card value and suit
     * @throws IllegalArgumentException if the specified shorthand is in illegal format, or doesn't represent a valid card
     */
    public Card(String shorthand) throws IllegalArgumentException {
        if (shorthand == null || shorthand.length() < 2) throw new IllegalArgumentException("Invalid shorthand provided: "+shorthand);
        value = VALUE.getByKey(shorthand.substring(0,shorthand.length()-1));
        suit = SUIT.getByKey(shorthand.substring(shorthand.length()-1));
    }

    /**
     * Constructs a Card representing the given cardValue and cardSuit
     * @param cardValue enum representing the (numeric) cardValue of the Card
     * @param cardSuit enum representing the cardSuit of the Card
     */
    public Card(VALUE cardValue, SUIT cardSuit) {
        value = cardValue;
        suit = cardSuit;
    }

    @Override
    public int compareTo(Card card) {
        // comparisons are based on value only
        return value.ordinal() - card.value.ordinal();
    }

    @Override
    public boolean equals(Object card) {
        if (card == this) return true;
        if (card == null || !(card instanceof Card)) return false;
        // equality requires both suit and value equality
        return ((Card) card).suit == suit && ((Card) card).value == value;
    }

    @Override
    public int hashCode() {
        // simple hash code function with randomly chosen prime number 7
        int result = 7 * value.hashCode() + suit.hashCode();
        return result;
    }

    @Override
    public String toString() {
        // some improved String representation for command line clarity
        StringBuilder sb = new StringBuilder();
        sb.append(value.mKeys[0]);
        sb.append(suit.mKey);
        return sb.toString().toUpperCase();
    }
}
