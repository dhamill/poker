package com.gingerman.model;

import java.util.*;

/**
 * Represents a five card poker hand
 */
public class Hand implements Comparable<Hand> {
    private static final int HAND_SIZE = 5;
    // Data structure for cards in this Hand is a list for easy access, duplicates removed, sorted highest card first
    public List<Card> cards;
    // Ordered list of Cards based on importance for comparisons between hands (biggest card in biggest set first, etc)
    public List<Card> comparisonCards = new ArrayList<Card>();
    public RANK rank;

    // Possible hand rankings
    public enum RANK {
        HIGH_CARD,
        ONE_PAIR,
        TWO_PAIR,
        THREE_OF_A_KIND,
        STRAIGHT,
        FLUSH,
        FULL_HOUSE,
        FOUR_OF_A_KIND,
        STRAIGHT_FLUSH,
        ROYAL_FLUSH
    }

    /**
     * Accepts a shorthand String representation of a hand containing five valid, unique Cards
     * @param shorthand comma delimited Card shorthand Strings specifying a 5 card hand
     * @throws IllegalArgumentException if shorthand is in illegal format, or does not represent a valid five Card set.
     */
    public Hand(String shorthand) throws IllegalArgumentException {
        if (shorthand == null || shorthand.length() == 0) throw new IllegalArgumentException("shorthand String was not specified");
        String[] shorthands = shorthand.split(",");

        // parse out the cards from the shorthand
        List<Card> unsortedCards = new ArrayList<Card>(HAND_SIZE);
        for (String cardShorthand : shorthands) {
            unsortedCards.add(new Card(cardShorthand));
        }
        organizeHand(unsortedCards);
    }

    /**
     * Constructs a Hand containing five valid, unique Cards
     * @param handCards the Cards to be placed into this Hand
     * @throws IllegalArgumentException if the input is not a valid five Card set.
     */
    public Hand(final List<Card> handCards) throws IllegalArgumentException {
        organizeHand(handCards);
    }

    /**
     * Sort the hand, and store it in an order-safe, easy access List. High card will be the first item.
     * @param newCards incoming cards to be sorted
     * @throws IllegalArgumentException if the hand is not a valid five Card set.
     */
    private void organizeHand(final List<Card> newCards) throws IllegalArgumentException {
        if (newCards == null) throw new IllegalArgumentException("Cannot define a Hand with null set of Cards");

        cards = new ArrayList<Card>(new HashSet<Card>(newCards)); // place into set to remove duplicates first

        // check restriction on Hand size
        if (cards.size() != HAND_SIZE) throw new IllegalArgumentException("Specified Hand does not contain enough (unique) Cards");

        // sort cards in hand for easier processing/comparing, high card will be first
        Collections.sort(cards, Collections.reverseOrder());

        // determine the rank of this hand, for comparison purposes
        determineRank();
    }

    /**
     * Full logic to determine which rank this hand is (full house, flush, etc)
     * We will also take the opportunity to pull out details about how to compare this hand to another by making note
     * of prioritized Cards to compare with same-ranked Hands, starting with the highest card in the biggest set of
     * same-valued cards, followed by the next highest card in the next biggest set, on down.
     */
    private void determineRank() {
        // default
        rank = RANK.HIGH_CARD;
        // Logic:
        // We are building a list of prioritized Cards so we can compare values between two hands, one at a time, to
        //  determine the winning hand. For example, a hand with two pairs would prioritize the highest valued pair,
        //  then the lower valued pair, then the high card remaining.
        // If 4 of a kind is found, it's the only matching group, and it gets top priority
        // If a 3 of a kind is found, it's the only matching group of size 3, and it gets top priority
        // If a 2 of a kind is found, it may be lower priority than a group of 3 or another 2. Use booleans to test.
        // If a 1 of a kind is found, throw it on the end of the list, highest order of single cards happens naturally.
        boolean haveDouble = false; // have we found a pair
        boolean haveTriple = false; // have we found a three of a kind
        boolean possibleFlush = true; // so far, all cards are the same suit
        boolean possibleStraight = true; // so far, all cards are sequential
        Card lastCard = null;
        int sizeOfMultiple = 1;
        for (Card card : cards) {
            if (lastCard != null) {
                // check if we could have a flush or straight
                possibleFlush = possibleFlush && card.suit == lastCard.suit;
                possibleStraight = possibleStraight && (card.value.ordinal() == lastCard.value.ordinal() - 1);

                if (card.compareTo(lastCard) == 0) {
                    sizeOfMultiple++;
                    if (cards.indexOf(card) != HAND_SIZE-1) {
                        lastCard = card;
                        continue; // skip logic below
                    }
                }
                if (sizeOfMultiple == 1) {
                    // remember the card, at lowest priority
                    comparisonCards.add(lastCard);
                } else if (sizeOfMultiple == 2) {
                    // determine priority of this card, insert into our comparison deck
                    comparisonCards.add(haveDouble || haveTriple ? 1 : 0, lastCard);
                    // special case, full house found
                    if (haveTriple) {
                        rank = RANK.FULL_HOUSE;
                        return;
                    }
                    // set the best rank we know of, and continue
                    rank = haveDouble ? RANK.TWO_PAIR : RANK.ONE_PAIR;
                    haveDouble = true;
                } else {
                    // either three of four of a kind, either way, top priority
                    comparisonCards.add(0, lastCard);
                    if (sizeOfMultiple == 3) {
                        // set the best rank we know of, and continue
                        rank = haveDouble ? RANK.FULL_HOUSE : RANK.THREE_OF_A_KIND;
                        haveTriple = true;
                    } else {
                        rank = RANK.FOUR_OF_A_KIND;
                    }
                }
                if (cards.indexOf(card) == HAND_SIZE-1) comparisonCards.add(card);
                sizeOfMultiple = 1; // reset
            }
            lastCard = card;
        }
        // Finalize rankings based on flush and straight, which will take priority if they apply
        if (possibleFlush && possibleStraight) rank = cards.get(0).value == Card.VALUE.ACE ? RANK.ROYAL_FLUSH : RANK.STRAIGHT_FLUSH;
        else if (possibleFlush) rank = RANK.FLUSH;
        else if (possibleStraight) rank = RANK.STRAIGHT;
    }

    /**
     * Compare Hand's according to rank, falling back on highest priority cards.
     */
    @Override
    public int compareTo(Hand hand) {
        if (rank != hand.rank) return rank.ordinal() - hand.rank.ordinal(); // easy case, rankings tell the whole story

        List<Card> inCompareCards = hand.comparisonCards;
        // iterate the prioritized comparison cards, comparing each index in turn, until one is higher or end is reached
        for (int i = 0; i < comparisonCards.size(); i++) {
            int compareNextHighest = comparisonCards.get(i).compareTo(inCompareCards.get(i));
            if (compareNextHighest != 0) return compareNextHighest;
        }
        return 0; // if they are both royal flushes, for example, they are equal
    }

    @Override
    public boolean equals(Object hand) {
        if (hand == this) return true;
        if (hand == null || !(hand instanceof Hand)) return false;

        Hand comparingHand = (Hand) hand;
        // equality is based on Cards within the Hand
        return cards.equals(comparingHand.cards);
    }

    @Override
    public int hashCode() {
        return cards.hashCode();
    }

    @Override
    public String toString() {
        // some improved String representation for command line clarity
        StringBuilder sb = new StringBuilder();
        sb.append(rank);
        sb.append(": {");
        for (Card c : cards) {
            sb.append(c.toString());
            sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);
        sb.append("}");
        return sb.toString();
    }
}
