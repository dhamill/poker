package com.gingerman;

import com.gingerman.model.Hand;

public class Poker {

    public static void main(String[] args) {
        String handInstructions = "\nHands are specified by a comma delimited String of value and suit pairs. Example: \"10c,jh,qd,ks,ad\" (quotes optional)\n";
	    if (args.length != 2) {
            System.out.println("to invoke: \njava Poker [Hand Hand]\n"+handInstructions);
            return;
        }

        Hand hand1 = null;
        try {
            hand1 = new Hand(args[0].replaceAll("\"",""));
        } catch (IllegalArgumentException e) {
            StringBuilder sb = new StringBuilder();
            sb.append("Illegal specification of first Hand: ");
            sb.append(args[0]);
            sb.append("\nError details: ");
            sb.append(e.getMessage());
            sb.append(handInstructions);
            System.err.print(sb.toString());
            return;
        }
        Hand hand2 = null;
        try {
            hand2 = new Hand(args[1].replaceAll("\"",""));
        } catch (IllegalArgumentException e) {
            StringBuilder sb = new StringBuilder();
            sb.append("Illegal specification of second Hand: ");
            sb.append(args[1]);
            sb.append("\nError details: ");
            sb.append(e.getMessage());
            sb.append(handInstructions);
            System.err.print(sb.toString());
            return;
        }

        try {
            Hand winner = findWinner(hand1, hand2);
            if (winner == null) System.out.println(String.format("%s is equal to %s", hand1.toString(), hand2.toString()));
            else {
                Hand loser = winner.equals(hand2) ? hand1 : hand2;
                System.out.println(String.format("%s beats %s", winner.toString(), loser.toString()));
            }

        } catch (IllegalArgumentException e) {
            System.err.print("Failure occurred: "+e.getMessage());
        }
    }

    /**
     * Determine which poker hand would win
     * @param a First poker hand to test
     * @param b Second poker hand to test
     * @return a or b, if one wins, or null if neither wins (tie)
     */
    public static Hand findWinner(Hand a, Hand b) {
        int compareResult = a.compareTo(b);
        if (compareResult == 0) return null;
        if (compareResult < 0) return b;
        return a;
    }
}
