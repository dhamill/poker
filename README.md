This application provides a command line tool, as well as junit tests that can be run within an IDE (built and tested with IntelliJ), which can compare and declare a winner between two hands of poker.

A card is specified using a shorthand string format containing a one letter suit specifier, prepended with a numeric or letter specifying the face value of the card.
Card values can be specified with their alpha initials (a == ace, k == king, q == queen, j == jack), or the numeric value they represent (14 == ace, 13 == king, etc).

_**Examples:**_

"**9c**" 	represents the Nine of Clubs.

"**as**" 	represents the Ace of Spades.

"**14s**" 	represents the Ace of Spades.


A hand is specified using a shorthand, comma delimited string format, and any quotes on the command line are ignored, and no whitespace is allowed.
The order of the cards specified is not important, so long as five unique cards are specified.

_**Examples:**_

"**as,ks,qs,js,10s**" represents a Royal Flush in Spades.

"**4c,4h,4d,3s,3h**" represents a Full House, 4's over 3's.


The command code can be compiled from command line in the root directory with the following command:

```
> javac src/com/gingerman/model/Card.java src/com/gingerman/model/Hand.java src/com/gingerman/Poker.java
```

The command line is invoked from the root directory as:

```
> java -classpath src com.gingerman.Poker as,ks,qs,js,10s 4c,4h,4d,3s,3h
```

The result will be something like:

```
ROYAL_FLUSH: {AS,KS,QS,JS,10S} beats FULL_HOUSE: {4H,4D,4C,3H,3S}
```

Or in the case of a tie:

```
FULL_HOUSE: {4C,4S,4H,3H,3S} is equal to FULL_HOUSE: {4C,4D,4H,3H,3S}
```